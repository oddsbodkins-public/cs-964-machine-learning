# Reference on OCR text
# https://pyimagesearch.com/2020/08/24/ocr-handwriting-recognition-with-opencv-keras-and-tensorflow/

import numpy as np
import pickle
import imutils
from imutils.contours import sort_contours
import cv2

# plt.imread("images/scanned-images/pages-1.jpg")
def predictAnImage(modelFilePath, numPyImageArray):

    # load pre-trained model
    with open(modelFilePath, "rb") as fid:
        ncaPipelineLoaded = pickle.load(fid)

    grayscaleImage = cv2.cvtColor(numPyImageArray, cv2.COLOR_BGR2GRAY)
    blurredImage = cv2.GaussianBlur(grayscaleImage, (5, 5), 0)

    # perform edge detection, find contours in the edge map,
    edgeDetectionImage = cv2.Canny(blurredImage, 30, 150)
    imageContours = cv2.findContours(edgeDetectionImage.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # sort the resulting contours from left-to-right
    imageContours = imutils.grab_contours(imageContours) 
    imageContours = sort_contours(imageContours, method="left-to-right")[0]

    # list of contour bounding boxes and associated characters that will be predicted
    charactersList = []

    # loop over the contours
    for contour in imageContours:
	    # compute the bounding box of the contour
	    (x, y, width, height) = cv2.boundingRect(contour)

	    # filter out bounding boxes, ensuring they are neither too small, nor too large
	    if (width >= 5 and width <= 150) and (height >= 15 and height <= 120):
		    # extract the character and threshold it to make the character
		    # appear as *white* (foreground) on a *black* background, then
		    # grab the width and height of the thresholded image
		    roi = grayscaleImage[y:y + height, x:x + width]
		    imageThreshold = cv2.threshold(roi, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
		    (tH, tW) = imageThreshold.shape

		    # resize the image, if the width is greater than the height
		    if tW > tH:
			    imageThreshold = imutils.resize(imageThreshold, width=28)
		    else: # or if the height is greater than the width
			    imageThreshold = imutils.resize(imageThreshold, height=28)

	        # get the image size after resizing with threshold 
		    # pad the width and height, force image to be 28 pixels x 28 pixels 
		    (tH, tW) = imageThreshold.shape
		    dX = int(max(0, 28 - tW) / 2.0)
		    dY = int(max(0, 28 - tH) / 2.0)

		    padded = cv2.copyMakeBorder(imageThreshold, top = dY, bottom = dY, left = dX, right = dX, borderType = cv2.BORDER_CONSTANT, value = (0, 0, 0))
		    padded = cv2.resize(padded, (28, 28))

		    # prepare the image for the scikit-learn classifier
		    #padded = padded.astype("float32") / 255.0
		    #padded = np.expand_dims(padded, axis=-1)

		    # update our list of characters that will be predicted
		    charactersList.append((padded, (x, y, width, height)))
		    
    return charactersList
		    
    # get the green bounding box image
    #for (padded, (x, y, width, height)) in charactersList:
    #    print("x: %f, y: %f, width: %f, height: %f" % (x, y, width, height) )
    #    #print("padded: %f" % (padded) );
    #    print("Prediction: %f" % ( ncaPipelineLoaded.predict(padded) ) )		    
		    
		    
    # extract the bounding box locations and padded characters
    #boundingBoxes = [b[1] for b in charactersList]
    #charactersList = np.array([contour[0] for contour in charactersList], dtype="float32")

