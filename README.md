Example Python desktop application with a machine learning model. 

* Basic usage of Jupyter Notebooks
* Python 3.11.2
* Cross-platform. Tested on Debian Linux 12 and Windows 10
* Application was bundled using PyInstaller.
* wxPython (wxwidgets) for GUI. https://wxpython.org/
