# PyInstaller
# https://stackoverflow.com/questions/20602727/pyinstaller-generate-exe-file-folder-in-onefile-mode
# https://python.land/virtual-environments/virtualenv#How_to_create_a_Python_venv
# https://realpython.com/pyinstaller-python/#limitations

# Machine learning modules

import imutils
from imutils.contours import sort_contours
import cv2
import pandas as pd
import numpy as np

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from tensorflow.keras.optimizers import Adam
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras import backend as K
from keras.utils import np_utils
from tensorflow.keras.models import load_model
from sklearn.model_selection import train_test_split

# Other modules

import shutil;
import tempfile;
import os;
import wx;

class MainWindow(wx.Frame):

    def __init__(self, parent, title):    
        self.firstImageSelected = True;
        
        # load the machine learning data / algorithms
        self.__initializeMachineLearningData();
    
        wx.Frame.__init__(self, parent, title=title, size=(1200, 800) );

        # top level panel
        rootPanel = wx.Panel(self);        
        
        #self.control = wx.TextCtrl(self, style=wx.TE_MULTILINE);
        self.CreateStatusBar(); # A Statusbar in the bottom of the window

        # Setting up the menu.
        fileMenu = wx.Menu();
        helpMenu = wx.Menu();

        # wx.ID_ABOUT and wx.ID_EXIT are standard IDs provided by wxWidgets.
        fileAboutMenuItem = helpMenu.Append(wx.ID_ABOUT, "&About"," Information about this program");
        self.Bind(wx.EVT_MENU, self.OnFileMenuAboutMenuItemEvent, fileAboutMenuItem); # set event handler
        
        fileMenu.AppendSeparator();
        fileOpenMenuItem = fileMenu.Append(wx.ID_OPEN,"O&pen image...","Load an image");        
        fileExitMenuItem = fileMenu.Append(wx.ID_EXIT,"E&xit"," Terminate the program");
        self.Bind(wx.EVT_MENU, self.OnFileMenuExitMenuItemEvent, fileExitMenuItem); # set event handler
        self.Bind(wx.EVT_MENU, self.OnFileMenuOpenMenuItemEvent, fileOpenMenuItem);

        # Creating the menubar.
        menuBar = wx.MenuBar();
        menuBar.Append(fileMenu, "&File"); # Adding the "fileMenu" to the MenuBar
        menuBar.Append(helpMenu, "&Help");
        
        self.SetMenuBar(menuBar);  # Adding the MenuBar to the Frame content.
        self.Show(True);
        
        # Window Body Content


        # Top section
        
        windowFrameContainerBoxSizer = wx.BoxSizer(wx.VERTICAL);

        imageSelectionContainerBoxSizer = wx.BoxSizer(wx.VERTICAL);
        
        openImageIconFilename = self.__getResourcePath(os.path.join ("images", "gnome-adwaita-icon-font-x-generic.png"));
        openImageIconBitmap = wx.Bitmap(openImageIconFilename, wx.BITMAP_TYPE_PNG);
        openImageIconBitmap = self.__scaleBitmap(openImageIconBitmap, 48, 48);
        
        openImageButtonFont = self.GetFont();
        openImageButtonFont.MakeLarger();
        
        openImageButton = wx.Button(rootPanel, id = wx.ID_ANY, label="Select an image with text characters...  ", style = wx.BU_LEFT | wx.RIGHT );
        openImageButton.SetBitmap(openImageIconBitmap);
        openImageButton.SetFont(openImageButtonFont);      
        openImageButton.Bind(wx.EVT_BUTTON, self.OnFileMenuOpenMenuItemEvent);
        
        uploadImageWarningLabel = wx.StaticText(rootPanel, label="Note:\n* Only characters A to Z are supported.\n* Letters must be capitalized.\n* The model does worse with handwritten text.\n");
        uploadImageWarningLabel.SetForegroundColour( (255,0,0) );
        
        # Separator line to make layout more organized
        separatorLine01 = wx.StaticLine(rootPanel, -1, (0, 0), (1170, 5));

        imageSelectionContainerBoxSizer.Add(openImageButton, 0, wx.TOP | wx.LEFT, 10);
        imageSelectionContainerBoxSizer.Add(uploadImageWarningLabel, 0, wx.TOP | wx.LEFT | wx.BOTTOM, 10);
        imageSelectionContainerBoxSizer.Add(separatorLine01, 0, wx.LEFT | wx.BOTTOM, 10);

        # Bottom section

        self.placeHolderImageWidth = 450;
        self.placeHolderImageHeight = 350;


        imageLeftContainerBoxSizer = wx.BoxSizer(wx.VERTICAL);
        imageLeftPanel = wx.Panel(rootPanel, size=(self.placeHolderImageWidth, self.placeHolderImageHeight));
        imageLeftLabel = wx.StaticText(rootPanel, label="Original image:");        
        
        leftSideImageFilename = self.__getResourcePath(os.path.join ("images", "onlinewebfonts-image-placeholder.png")); # openclipart-pleasant-verdant-landscape-mosaic.png";
        rightSideImageFilename = self.__getResourcePath(os.path.join ("images", "onlinewebfonts-image-placeholder.png"));
        
        leftImageBitmap = None;
        leftImageBitmap = wx.Bitmap(leftSideImageFilename); # Create a bitmap, load from file path
        leftImageBitmap = self.__scaleBitmap(leftImageBitmap, self.placeHolderImageWidth, self.placeHolderImageHeight);
        
        self.originalImageBitmap = wx.StaticBitmap(imageLeftPanel, -1, leftImageBitmap);
        self.originalImageBitmap.SetPosition( (0, 0) );

        imageLeftContainerBoxSizer.Add(imageLeftLabel, 0, wx.LEFT | wx.BOTTOM, 10);
        imageLeftContainerBoxSizer.Add(imageLeftPanel, 1, wx.SHAPED | wx.LEFT | wx.TOP, 10);
        

        
        imageRightContainerBoxSizer = wx.BoxSizer(wx.VERTICAL);
        imageRightPanel = wx.Panel(rootPanel, size=(self.placeHolderImageWidth, self.placeHolderImageHeight));
        imageRightLabel = wx.StaticText(rootPanel, label="Prediction image:");
        
        rightImageBitmap = None;
        rightImageBitmap = wx.Bitmap(rightSideImageFilename);        
        rightImageBitmap = self.__scaleBitmap(rightImageBitmap, self.placeHolderImageWidth, self.placeHolderImageHeight);

        self.predictedImageBitmap = wx.StaticBitmap(imageRightPanel, -1, rightImageBitmap);
        self.predictedImageBitmap.SetPosition( (0, 0) );

        imageRightContainerBoxSizer.Add(imageRightLabel, 0, wx.BOTTOM, 10);
        imageRightContainerBoxSizer.Add(imageRightPanel, 1, wx.SHAPED | wx.LEFT | wx.TOP, 10);
                
        imageParentContainerGridSizer = wx.GridSizer(cols=2, vgap=10, hgap=10);
        imageParentContainerGridSizer.Add(imageLeftContainerBoxSizer, 0, 0, 0);
        imageParentContainerGridSizer.Add(imageRightContainerBoxSizer, 0, wx.LEFT, 50);


        belowRightImageSectionContainerBoxSizer = wx.BoxSizer(wx.VERTICAL);
        
        self.imagePredictionTextField = None;
        
        imagePredictionTextLabel = wx.StaticText(rootPanel, label="Predicted text:");
        self.imagePredictionTextField = wx.TextCtrl(rootPanel, style=wx.TE_READONLY | wx.TE_MULTILINE, size=(1160, 100));
        self.imagePredictionTextField.SetMargins(left=5, top=5);
        self.imagePredictionTextField.SetValue("Please select an image.");
        
        belowRightImageSectionContainerBoxSizer.Add(imagePredictionTextLabel, 0, wx.LEFT | wx.BOTTOM, 10);
        belowRightImageSectionContainerBoxSizer.Add(self.imagePredictionTextField, 1, wx.LEFT, 10);

        # Separator line to make layout more organized
        separatorLine02 = wx.StaticLine(rootPanel, -1, (0, 0), (1170, 5));


        windowFrameContainerBoxSizer.Add(imageSelectionContainerBoxSizer, 0, 0, 0);        
        windowFrameContainerBoxSizer.Add(imageParentContainerGridSizer, 0, 0, 0);
        
        windowFrameContainerBoxSizer.Add(separatorLine02, 0, wx.LEFT | wx.TOP | wx.BOTTOM, 10);
        windowFrameContainerBoxSizer.Add(belowRightImageSectionContainerBoxSizer, 0, 0, 0);

        rootPanel.SetSizer(windowFrameContainerBoxSizer);
        rootPanel.Layout();
        #self.SetSizer(windowFrameContainerBoxSizer);

    # PyInstaller moves folders and files, this helps find them again
    def __getResourcePath(self, relative_path):
    # Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)


    def __initializeMachineLearningData(self):
        self.machineLearningModel = load_model(self.__getResourcePath(os.path.join ("model", "jupyter-notebook-tensorflow-model.model")));

        # Debugging information
        # print(model.summary());

    def __loadMachineLearningData(self, imageFilePath):
        
        # load the input image from disk, convert it to grayscale, and blur it to reduce noise        
        image = cv2.imread(imageFilePath);
        if image is None:
            print("An error occurred while attempting to load an image file.");
            return;
            
        else:
        
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            blurred = cv2.GaussianBlur(gray, (5, 5), 0)

            # perform edge detection, find contours in the edge map, and sort the
            # resulting contours from left-to-right
            edged = cv2.Canny(blurred, 30, 150)
            cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
                cv2.CHAIN_APPROX_SIMPLE)
            cnts = imutils.grab_contours(cnts)
            cnts = sort_contours(cnts, method="left-to-right")[0]

            # initialize the list of contour bounding boxes and associated
            # characters that we'll be OCR'ing
            chars = []

            # loop over the contours
            for c in cnts:
                # compute the bounding box of the contour
                (x, y, w, h) = cv2.boundingRect(c)

                # filter out bounding boxes, ensuring they are neither too small
                # nor too large
                if (w >= 5 and w <= 150) and (h >= 15 and h <= 120):
                    # extract the character and threshold it to make the character
                    # appear as *white* (foreground) on a *black* background, then
                    # grab the width and height of the thresholded image
                    roi = gray[y:y + h, x:x + w]
                    thresh = cv2.threshold(roi, 0, 255,
	                    cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
                    (tH, tW) = thresh.shape

                    # if the width is greater than the height, resize along the
                    # width dimension
                    if tW > tH:
	                    thresh = imutils.resize(thresh, width=28)

                    # otherwise, resize along the height
                    else:
	                    thresh = imutils.resize(thresh, height=28)
	                    
                    # re-grab the image dimensions (now that its been resized)
                    # and then determine how much we need to pad the width and
                    # height such that our image will be 32x32
                    (tH, tW) = thresh.shape
                    dX = int(max(0, 28 - tW) / 2.0)
                    dY = int(max(0, 28 - tH) / 2.0)

                    # pad the image and force 32x32 dimensions
                    padded = cv2.copyMakeBorder(thresh, top=dY, bottom=dY,
	                    left=dX, right=dX, borderType=cv2.BORDER_CONSTANT,
	                    value=(0, 0, 0))
                    padded = cv2.resize(padded, (28, 28))

                    # prepare the padded image for classification via our
                    # handwriting OCR model
                    padded = padded.astype("float32") / 255.0
                    padded = np.expand_dims(padded, axis=-1)

                    # update our list of characters that will be OCR'd
                    chars.append((padded, (x, y, w, h)))
                    
            # extract the bounding box locations and padded characters
            boxes = [b[1] for b in chars]
            chars = np.array([c[0] for c in chars], dtype="float32")

            # OCR the characters using our handwriting recognition model
            preds = self.machineLearningModel.predict(chars)

            # define the list of label names
            labelNames = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            labelNames = [l for l in labelNames]
            
            shortenedImageFileName = imageFilePath;
            fileNameBase = os.path.basename(imageFilePath);
            fileNameBase = os.path.splitext(fileNameBase);
            
            if len(fileNameBase) >= 2:
                shortenedImageFileName = ("%s%s" % (fileNameBase[0], fileNameBase[1]) );
            
            predictedText = ("Predicted text for image: '%s'\n" % (shortenedImageFileName) );

            # loop over the predictions and bounding box locations together
            for (pred, (x, y, w, h)) in zip(preds, boxes):
                # find the index of the label with the largest corresponding
                # probability, then extract the probability and label
                i = np.argmax(pred)
                prob = pred[i]
                label = labelNames[i]
                predictedText += label;

                # draw the prediction on the image
                print("[INFO] {} - {:.2f}%".format(label, prob * 100))
                cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.putText(image, label, (x - 10, y - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 255, 0), 2)
            
            
            predictedText += "\n"
            
            # Save prediction image in temporary file: https://docs.python.org/3/library/tempfile.html#tempfile.TemporaryFile
            tempFilePath = "";

            try: 
                with tempfile.NamedTemporaryFile(mode='w+t', delete=False) as tempFile:
                    print(tempFile.name);
                    tempFile.write("Foo");
                    tempFilePath = tempFile.name;
                    tempFile.close();
                    shutil.copy(tempFilePath, tempFilePath + ".jpg");
                
            except:
                print("An exception occurred while opening a temporary file.");


            try:
                    print("Does it exist: %s" % (os.path.isfile(tempFilePath + ".jpg")));
                    print("Writing to temporary file: %s" % (tempFilePath + ".jpg") );
                    cv2.imwrite(tempFilePath + ".jpg", image);
            except:
                print("An exception occurred while opening a temporary file.");


            
            predictedImageBitmap = wx.Bitmap(tempFilePath + ".jpg");
            predictedImageBitmap = self.__scaleBitmap(predictedImageBitmap, self.placeHolderImageWidth, self.placeHolderImageHeight);

            self.predictedImageBitmap.SetBitmap(predictedImageBitmap);
            
            existingStrings = "";
            
            # get existing strings
            if self.firstImageSelected == True:
                self.firstImageSelected = False;
                
                existingStrings += ("%s" % (predictedText) );
            else:
                existingStrings = self.imagePredictionTextField.GetValue();            
                existingStrings += ("\n%s" % (predictedText) )
            
            # update text field with the predicted text
            self.imagePredictionTextField.SetValue(existingStrings);
        
              
 
    # Resize an image if necessary. A user can select an image with unknown aspect ratio
    # https://stackoverflow.com/questions/2504143/how-to-resize-and-draw-an-image-using-wxpython
    def __scaleBitmap (self, bitmap, maxWidth, maxHeight):
        image = bitmap.ConvertToImage();
        width = image.GetWidth();
        height = image.GetHeight();

        # calculate a  new width and height based on an image aspect ratio
        # https://math.stackexchange.com/questions/180804/how-to-get-the-aspect-ratio-of-an-image
                
        if width > height: # larger width than height
            adjustedWidth = maxWidth;
            adjustedHeight = int(maxWidth * (height / width)); # computes a float, cast to integer
        elif height > width: # larger height than width
            adjustedHeight = maxHeight;
            adjustedWidth = int(maxHeight * (width / height)); # computes a float, cast to integer
        elif width == height: # width = height
            adjustedWidth = maxWidth;
            adjustedHeight = maxHeight;

        # Debugging information
        # print("max width: %d height: %d" % (maxWidth, maxHeight) );        
        # print("new width: %d height: %d" % (adjustedWidth, adjustedHeight) );
        
        image = image.Scale(adjustedWidth, adjustedHeight, wx.IMAGE_QUALITY_HIGH);
        result = wx.Bitmap(image);
        return result;

    # menu item event handler
    def OnFileMenuAboutMenuItemEvent(self, event):
        dialog = wx.MessageDialog( self, "\nMachine Learning Project using Python\n\nName:\t\John Smith\nAssignment:\tC964 Python Machine Learning\n", "About this application", wx.OK);
        dialog.ShowModal();
        dialog.Destroy();

    def OnFileMenuExitMenuItemEvent(self, event):
        self.Close(True);
        
    def OnFileMenuOpenMenuItemEvent(self, event):
        # Load an image / Open a file
        directoryName = "";
        fileName = "";
        fileDialogPath = self.__getResourcePath("images");
        w = "JPEG / JPG files (*.jpeg;*.jpg)|*.jpg|GIF files (*.gif)|*.gif|PNG files (*.png)|*.png";
        
        with wx.FileDialog(self, "Select an image file", fileDialogPath, wildcard=w, style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST | wx.FD_PREVIEW) as fileDialog:
        
            userSelectionEvent = fileDialog.ShowModal();

            # The user wants to cancel / exit the open afile prompt
            if userSelectionEvent == wx.ID_CANCEL:
                return;
                    
            # If the "OK" button is clicked, event
            if userSelectionEvent == wx.ID_OK:
                fileName = fileDialog.GetFilename();
                directoryName = fileDialog.GetDirectory();
                fullPath = os.path.join (directoryName, fileName);
                
                print("Attempting to open file '%s'" % (fullPath) );

                # update the images
                try:                
                    with open(fullPath, "r") as selectedFile:
                    
                        selectedImageBitmap = wx.Bitmap(fullPath);
                        selectedImageBitmap = self.__scaleBitmap(selectedImageBitmap, self.placeHolderImageWidth, self.placeHolderImageHeight);
                        
                        self.originalImageBitmap.SetBitmap(selectedImageBitmap);
                        #self.predictedImageBitmap.SetBitmap(selectedImageBitmap);
                        
                        # attempt to predict the text
                        self.__loadMachineLearningData(fullPath);               
                    
                    selectedFile.close();
                except IOError:
                    wx.LogError("Could not open file '%s'." % (fullPath) );

                # update the images
                #testImageFilename = self.__getResourcePath(os.path.join ("images", "openclipart-pleasant-verdant-landscape-mosaic.png"));                
                #testImageBitmap = wx.Bitmap(testImageFilename);
                #testImageBitmap = self.__scaleBitmap(testImageBitmap, self.placeHolderImageWidth, self.placeHolderImageHeight);
                
                #self.originalImageBitmap.SetBitmap(testImageBitmap);
                #self.predictedImageBitmap.SetBitmap(testImageBitmap);         
                
                try:                
                    with open(fullPath, "r") as selectedFile:               
                        #self.control.SetValue (selectedFile.read ());
                        selectedFile.close();
                except IOError:
                    wx.LogError("Could not open file '%s'." % (fullPath) );
                                    
        fileDialog.Destroy(); # Close the select a file dialog modal window.

def main():
    app = wx.App(False);
    frame = MainWindow(None, "C964 - Python Machine Learning");
    app.MainLoop();
    
if __name__ == '__main__':
    main();
       
